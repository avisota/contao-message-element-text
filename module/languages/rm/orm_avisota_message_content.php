<?php

/**
 * Translations are managed using Transifex. To create a new translation
 * or to help to maintain an existing one, please register at transifex.com.
 *
 * @link    http://help.transifex.com/intro/translating.html
 * @link    https://www.transifex.com/projects/p/avisota-contao/language/rm/
 *
 * @license http://www.gnu.org/licenses/lgpl-3.0.html LGPL
 *
 * last-updated: 2014-03-11T16:03:27+01:00
 */

$GLOBALS['TL_LANG']['orm_avisota_message_content']['definePlain']['0'] = 'Definir il text senza formataziun';
$GLOBALS['TL_LANG']['orm_avisota_message_content']['definePlain']['1'] = 'Definir in text senza formataziun persunalisà u generar el dal html.';
$GLOBALS['TL_LANG']['orm_avisota_message_content']['plain']['0']       = 'Text senza formataziun';
$GLOBALS['TL_LANG']['orm_avisota_message_content']['plain']['1']       = 'Text senza formataziun da html.';
$GLOBALS['TL_LANG']['orm_avisota_message_content']['text']['0']        = 'Text';
$GLOBALS['TL_LANG']['orm_avisota_message_content']['text']['1']        = 'Ti pos utilisar tags da HTML per formatar il text.';
$GLOBALS['TL_LANG']['orm_avisota_message_content']['text_legend']      = 'Text';
