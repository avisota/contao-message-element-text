<?php

/**
 * Avisota newsletter and mailing system
 * Copyright © 2016 Sven Baumann
 *
 * PHP version 5
 *
 * @copyright  way.vision 2016
 * @author     Sven Baumann <baumann.sv@gmail.com>
 * @package    avisota/contao-message
 * @license    LGPL-3.0+
 * @filesource
 */

/**
 * Fields
 */
$GLOBALS['TL_LANG']['orm_avisota_message_content']['text']        = array(
    'Text',
    'You can use HTML tags to format the text.'
);
$GLOBALS['TL_LANG']['orm_avisota_message_content']['definePlain'] = array(
    'Define plain text',
    'Define custom plain text instead of generate it from html.'
);
$GLOBALS['TL_LANG']['orm_avisota_message_content']['plain']       = array(
    'Plain text',
    'Plain text without html formatting.'
);

/**
 * Legends
 */
$GLOBALS['TL_LANG']['orm_avisota_message_content']['text_legend'] = 'Text';
