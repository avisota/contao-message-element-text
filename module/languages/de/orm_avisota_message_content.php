<?php

/**
 * Translations are managed using Transifex. To create a new translation
 * or to help to maintain an existing one, please register at transifex.com.
 *
 * @link    http://help.transifex.com/intro/translating.html
 * @link    https://www.transifex.com/projects/p/avisota-contao/language/de/
 *
 * @license http://www.gnu.org/licenses/lgpl-3.0.html LGPL
 *
 * last-updated: 2014-03-25T14:17:31+01:00
 */

$GLOBALS['TL_LANG']['orm_avisota_message_content']['definePlain']['0'] = 'Einfachen Text definieren';
$GLOBALS['TL_LANG']['orm_avisota_message_content']['definePlain']['1'] = 'Erstellen Sie einfachen Text anstatt ihn als HTML zu erfassen.';
$GLOBALS['TL_LANG']['orm_avisota_message_content']['plain']['0']       = 'Einfacher Text';
$GLOBALS['TL_LANG']['orm_avisota_message_content']['plain']['1']       = 'Einfacher Text ohne HTML-Formatierung';
$GLOBALS['TL_LANG']['orm_avisota_message_content']['text']['0']        = 'Text';
$GLOBALS['TL_LANG']['orm_avisota_message_content']['text']['1']        = 'Zur Formatierung können Sie HTML-Tags verwenden-';
$GLOBALS['TL_LANG']['orm_avisota_message_content']['text_legend']      = 'Text';
